#pragma once
#include <string>
#include <iostream>
#include <string.h>

#define myPath "" //enter your path, if input.sig located in another folder

class LexerResult
{

public:

	LexerResult(std::string token, int code, int rowNumber, int columnNumber) {
		this->token = token;
		this->code = code;
		this->columnNumber = columnNumber;
		this->rowNumber = rowNumber;
	}

	~LexerResult() = default;

	inline std::string getToken() const { return token; }

	inline int getCode() const { return code; }

	inline int getRowNumber() const { return rowNumber; }

	inline int getColumnNumber() const { return columnNumber; }

	void print() {
        std::ofstream res;
        char* resName = "generated.txt";
        char result[100];
        strcpy(result,myPath);
        strcat(result,resName);
        res.open(result, std::ios::app);
        res << rowNumber << " " << columnNumber << " " << code << " " << token << std::endl;
        res.close();
		std::cout << rowNumber << " " << columnNumber << " " << code << " " << token << std::endl;
	}

	static void myError(int x, int letter, int currentRow, int currentColumn){
        std::ofstream res;
        char* resName = "generated.txt";
        char result[100];
        strcpy(result,myPath);
        strcat(result,resName);
        res.open(result, std::ios::app);

	    switch (x) {
        case 1:
            res << "ERROR_1 " << (char)letter << " code " << letter << " is undefined (row:" << currentRow << ", col:" << currentColumn << ")" << std::endl;
        break;
        case 3:
            res << "ERROR_3 " << (char)letter << " code " << letter << " is not number, whitespace or separator (row:"
					<< currentRow << ", col:" << currentColumn << ")" << std::endl;
        break;
        case 4:
            res << "ERROR_4 " << "unclose comment (row:" << currentRow << ", col:" << currentColumn << ")" << std::endl;
        break;

        }
        res.close();
	}

private:
    std::string token;
	int code;
	int rowNumber;
	int columnNumber;

};
