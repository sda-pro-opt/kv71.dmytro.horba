#include "Lexer.h"
#include <string>
#include <iostream>

typedef std::pair<std::string, int> MapPair;

int TOKEN_STATUS_CONSTANT = 0;
int TOKEN_STATUS_IDENTIFIER = 1;
int TOKEN_STATUS_RESERVED_WORD = 2;
int TOKEN_STATUS_ONE_SEPARATED_TOKEN = 3;
int TOKEN_STATUS_COMMENT = 4;

int oneSymbolTokenIndex = 0;
int reservedWordsIndex = 401;
int constantsIndex = 501;
int variablesIndex = 1001;

int currentTokenState[5] = {0, 0, 0, 0, 0};
std::string token = "";

int currentColumn = 0;
int currentRow = 1;
int savedColumn = currentColumn;
int savedRow = currentRow;
bool errorHappened = false;

bool Lexer::isLetter(int character) {
	for (auto& c : lettersVector) {
		if (c == character) {
			return true;
		}
	}
	return false;
}

bool Lexer::isNumber(int character) {
	for (auto& number : numbersVector) {
		if ((char) number == character) {
			return true;
		}
	}
	return false;
}

bool Lexer::isWhiteSpace(int character) {
	for (auto& whiteSpaceId : whiteSpacesVector) {
		if (whiteSpaceId == character) {
			return true;
		}
	}
	return false;
}

bool Lexer::isOneSeparated(int c) {
	for (auto& oneSeparatedToken : oneSeparatedTokens) {
		if (oneSeparatedToken == c) {
			return true;
		}
	}
	return false;
}


void Lexer::resetTokenStatus() {
	currentTokenState[0] = 0;
	currentTokenState[1] = 0;
	currentTokenState[2] = 0;
	currentTokenState[3] = 0;
	currentTokenState[4] = 0;
}

Lexer::Lexer() {
	addReservedWord("PROCEDURE");
	addReservedWord("BEGIN");
	addReservedWord("END");
	addReservedWord("CONST");
	addReservedWord("FLOAT");
	addReservedWord("INTEGER");

	addOneSeparatedToken(":");
	addOneSeparatedToken(";");
	addOneSeparatedToken("(");
	addOneSeparatedToken(")");
	addOneSeparatedToken(".");
	addOneSeparatedToken("-");
	addOneSeparatedToken("=");
}

void Lexer::addToken(std::string&) {
	if (currentTokenState[TOKEN_STATUS_CONSTANT] == 1) {
		addTokenToResultVector(token, CONSTANT, savedColumn, savedRow);
		return;
	}

	if (currentTokenState[TOKEN_STATUS_IDENTIFIER] == 1 && currentTokenState[TOKEN_STATUS_RESERVED_WORD] == 0) {
		addTokenToResultVector(token, IDENTIFIER, savedColumn, savedRow);
		return;
	}

	if (currentTokenState[TOKEN_STATUS_RESERVED_WORD] == 1 && currentTokenState[TOKEN_STATUS_IDENTIFIER] == 0) {
		addTokenToResultVector(token, RESERVED_WORD, savedColumn, savedRow);
		return;
	}

	if (currentTokenState[TOKEN_STATUS_ONE_SEPARATED_TOKEN] == 1) {
		addTokenToResultVector(token, ONE_SEPARATED_TOKEN, savedColumn, savedRow);
		return;
	}


	if (currentTokenState[TOKEN_STATUS_COMMENT] == 1) {
		return;
	}

	int reservedWordCode = isTokenReservedWord(token);
	if (reservedWordCode != -1) {
		addTokenToResultVector(token, RESERVED_WORD, savedColumn, savedRow);
		return;
	} else {
		addTokenToResultVector(token, IDENTIFIER, savedColumn, savedRow);
		return;
	}

}

int Lexer::addIdentifier(std::string& token) {
	identifiersTokensMap.insert(MapPair(token, variablesIndex));
	int code = variablesIndex;
	variablesIndex++;
	return code;
}

int Lexer::addConstant(std::string& word) {
	constantTokensMap.insert(MapPair(word, constantsIndex));
	int code = constantsIndex;
	constantsIndex++;
	return code;
}

void Lexer::addTokenToResultVector(std::string& token, TokenStatus status, int column, int row) {
	int code = -1;
	switch (status) {
	case IDENTIFIER:
		code = isTokenIdentifier(token);
		if (code == -1) {
			code = addIdentifier(token);
		}
		lexerResultValues.push_back(LexerResult(token, code, row, column));
		break;
	case RESERVED_WORD:
		code = isTokenReservedWord(token);
		lexerResultValues.push_back(LexerResult(token, code, row, column));
		break;
	case ONE_SEPARATED_TOKEN:
		code = isTokenOneSeparatedToken(token);
		lexerResultValues.push_back(LexerResult(token, code, row, column));
		break;
	case CONSTANT:
		code = isTokenConstant(token);
		if (code == -1) {
			code = addConstant(token);
		}
		lexerResultValues.push_back(LexerResult(token, code, row, column));
		break;
	}
}

void Lexer::addReservedWord(const char* word) {
	reservedWords.insert(MapPair(word, reservedWordsIndex));
	reservedWordsIndex++;
}

void Lexer::addOneSeparatedToken(const char* word) {
	oneSeparatedTokensMap.insert(MapPair(word, oneSymbolTokenIndex));
	oneSymbolTokenIndex++;
}

int Lexer::isTokenReservedWord(std::string& word){
	for (auto& resWord : reservedWords) {
		if (resWord.first == word) {
			return resWord.second;
		}
	}
	return -1;
}

int Lexer::isTokenOneSeparatedToken(std::string& word) {
	for (auto& oneSeparetadToken : oneSeparatedTokensMap) {
		if (oneSeparetadToken.first == word) {
			return oneSeparetadToken.second;
		}
	}
	return -1;
}

int Lexer::isTokenIdentifier(std::string& word) {
	for (auto& indentifier : identifiersTokensMap) {
		if (indentifier.first == word) {
			return indentifier.second;
		}
	}
	return -1;
}

int Lexer::isTokenConstant(std::string& word) {
	for (auto& constant : constantTokensMap) {
		if (constant.first == word) {
			return constant.second;
		}
	}
	return -1;
}

int Lexer::readCharacterFromFile(std::ifstream &file) {
	char currentCaracter = file.eof();
	if (!file.eof()) {
		file.get(currentCaracter);
		currentColumn++;
	}

	return (int)currentCaracter;
}

int Lexer::caseOneSeparated(int letter, std::ifstream& file) {
	resetTokenStatus();
	currentTokenState[TOKEN_STATUS_ONE_SEPARATED_TOKEN] = 1;
    token = (char)letter;
    addToken(token);
    token = "";
    resetTokenStatus();
    return readCharacterFromFile(file);


}

int Lexer::caseLetter(int letter, std::ifstream& file) {
	token += (char)letter;
	currentTokenState[TOKEN_STATUS_IDENTIFIER] = 1;
	currentTokenState[TOKEN_STATUS_RESERVED_WORD] = 1;

	while (!file.eof()) {
		int nextLetter = readCharacterFromFile(file);

		if (isNumber(nextLetter)) {
			currentTokenState[TOKEN_STATUS_IDENTIFIER] = 1;
			token += (char)nextLetter;
			continue;
		}

		if (!isLetter(nextLetter)) {
			if (!token.empty()) {
				addToken(token);
			}
			token = "";
			resetTokenStatus();
			return nextLetter;
		}

		token += (char)nextLetter;
	}
}

int Lexer::caseNumber(int letter, std::ifstream& file) {
	currentTokenState[TOKEN_STATUS_CONSTANT] = 1;
	token += (char)letter;

	while (!file.eof()) {
		int nextLetter = readCharacterFromFile(file);
		if (!isNumber(nextLetter)) {

			if (!isOneSeparated(nextLetter) && !isWhiteSpace(nextLetter)) {

				std::cout << "ERROR_3 " << (char)nextLetter << " code " << nextLetter << " is not number, whitespace or separator (row:"
					<< currentRow << ", col:" << currentColumn << ")" << std::endl;
                LexerResult::myError(3, nextLetter, currentRow, currentColumn);
				errorHappened = true;
				return nextLetter;
			}

			if (!token.empty()) {
				addToken(token);
			}
			token = "";
			resetTokenStatus();
			return nextLetter;
		}

		token += (char)nextLetter;
	}
}

int Lexer::caseComment(int letter, std::ifstream& file) {
	resetTokenStatus();
	currentTokenState[TOKEN_STATUS_COMMENT] = 1;
	token += "(";
	token += (char)letter;
	bool isStartFound = false;

	int nextLetter;


	while (!file.eof()) {
		nextLetter = readCharacterFromFile(file);

		if (nextLetter == '\0') {
			errorHappened = true;
			std::cout << "ERROR_4 " << "unclose comment (row:" << currentRow << ", col:" << currentColumn << ")" << std::endl;
			LexerResult::myError(4, letter, currentRow, currentColumn);
			return nextLetter;
		}

		if (nextLetter == '*') {
			isStartFound = true;
		} else if (nextLetter == ')' && isStartFound) {
			token += (char)nextLetter;
			addToken(token);
			token = "";
			resetTokenStatus();
			return readCharacterFromFile(file);
		} else {
			isStartFound = false;
		}

		if (nextLetter == '\n') { currentRow++; currentColumn = 0; }

		token += (char)nextLetter;

	}

}

bool Lexer::isComment(int letter) {
	return letter == '(';
}

void Lexer::scanFile(const char* filePath) {
	std::ifstream file;

	file.open(filePath, std::ios::in);

	int savedLetter = -1;
	bool isLastBracket = false;

	while (!file.eof()) {
		int letter = savedLetter == -1 ? readCharacterFromFile(file) : savedLetter;

        if (isLastBracket){//caseComment
            savedLetter = -1;
            if (letter == '*'){
                letter = caseComment(letter, file);
                if (errorHappened) return;
                isLastBracket = false;
            }else{
                resetTokenStatus();
                currentTokenState[TOKEN_STATUS_ONE_SEPARATED_TOKEN] = 1;
                token = '(';
                addToken(token);
                token = "";
                resetTokenStatus();
                if (errorHappened) return;
                isLastBracket = false;
            }
        }




		if (isLetter(letter)) {
			savedLetter = -1;
			savedColumn = currentColumn;
			savedRow = currentRow;
			letter = caseLetter(letter, file);
			if (errorHappened) return;
		}

		if (isOneSeparated(letter) && letter != '(') {
			savedLetter = -1;
			savedColumn = currentColumn;
			savedRow = currentRow;
			letter = caseOneSeparated(letter, file);
			if (errorHappened) return;
		}

		if (isNumber(letter)) {
			savedLetter = -1;
			savedColumn = currentColumn;
			savedRow = currentRow;
			letter = caseNumber(letter, file);
			if (errorHappened) return;
		}


		if (isWhiteSpace(letter)) {
			savedColumn = currentColumn;
			savedRow = currentRow;
			savedLetter = -1;
			if (!token.empty()) {
				addToken(token);
				token = "";
			}
			resetTokenStatus();
			if (letter == '\n') {
				currentColumn = 0;
				currentRow++;
			}
			continue;
		}

		if (letter == '('){
            savedLetter = -1;
			savedColumn = currentColumn;
			savedRow = currentRow;
			isLastBracket = true;
			letter = readCharacterFromFile(file);
        }

		// to prevent endless loop when met undefined letter
		if (letter == savedLetter) {
			std::cout << "ERROR_1 " << (char)letter << " code " << letter << " is undefined (row:" << currentRow << ", col:" << currentColumn << ")" << std::endl;
			LexerResult::myError(1, letter, currentRow, currentColumn);
			return;
		}

		savedLetter = letter;

	}

}

void Lexer::printScanResult() {
	for (auto& result : lexerResultValues) {
		result.print();
	}
}
