#pragma once
#include <vector>
#include <fstream>
#include <map>
#include "LexerResult.h"
#include "TokenStatus.h"



class Lexer {
private:

	std::vector<int> lettersVector = {
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
		'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
		'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z'
	};
	std::vector<int> numbersVector = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57};
	std::vector<int> whiteSpacesVector = {9, 10, 13, 32};
	std::vector<int> oneSeparatedTokens = {':', ';', '(',')','.','-','='};

	std::vector<LexerResult> lexerResultValues;

	std::map<std::string, int> reservedWords;
	std::map<std::string, int> oneSeparatedTokensMap;
	std::map<std::string, int> identifiersTokensMap;
	std::map<std::string, int> constantTokensMap;


	bool isLetter(int);

	bool isNumber(int);

	bool isComment(int);

	bool isWhiteSpace(int);

	bool isOneSeparated(int);

	void resetTokenStatus();

	void addToken(std::string&);

	void addReservedWord(const char*);

	void addOneSeparatedToken(const char*);

	int addIdentifier(std::string&);

	int addConstant(std::string&);

	void addTokenToResultVector(std::string&, TokenStatus, int, int);

	int isTokenReservedWord(std::string&);

	int isTokenOneSeparatedToken(std::string&);

	int isTokenIdentifier(std::string&);

	int isTokenConstant(std::string&);


	int readCharacterFromFile(std::ifstream&);


	int caseOneSeparated(int, std::ifstream&);

	int caseLetter(int, std::ifstream&);

	int caseNumber(int, std::ifstream&);

	int caseComment(int, std::ifstream&);

public:

	Lexer();
	~Lexer() = default;

	void scanFile(const char*);
	void printScanResult();
};
